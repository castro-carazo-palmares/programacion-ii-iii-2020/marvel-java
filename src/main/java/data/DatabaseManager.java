package data;

import entidades.*;
import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    private final String host = "localhost:3306";//Contiene el host de la BD.
    private final String database = "marvel";//Contiene el nombre de la BD.
    private final String url = "jdbc:mysql://" + host + "/" + database + "?autoReconnect=true&useSSL=false";//Contiene la dirección de la BD.
    private final String USER = "root";//Contiene el usuario de la BD.
    private final String PASS = "rootroot";//Contiene el password de la BD.
    private final String DRIVER = "com.mysql.jdbc.Driver";//Contiene la dirección del Driver de MySQL.
    Connection connection = null;//Inicializa la variable conexión a la BD.
    Statement statement = null;//Inicializa la variable statement de la BD.
    ResultSet resultSet = null;//Inicializa la variable tipo ResultSet.

    /**
     * Abre la Base de Datos.
     */
    public void abrirBD() {

        try {

            Class.forName(DRIVER);
            connection = (Connection) DriverManager.getConnection(url, USER, PASS);//Crea la conexión a la BD.
            statement = (Statement) connection.createStatement();//Crea el Statement.

        }//Fin de try
        catch (Exception ex) {

            System.err.println(ex);

        }//Fin de catch

    }//Fin de clase abrirBD

    public int insertarGenero(Genero genero) {
        ResultSet result;
        int id = -1;

        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "INSERT INTO Generos VALUES (?,?)";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, genero.getId());
            preparedStatement.setString(2, genero.getNombre());

            preparedStatement.executeUpdate();

            result = preparedStatement.getGeneratedKeys();

            if (result.first()) {
                id = result.getInt(1);
            }

            preparedStatement.close();

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return id;
    }

    public int insertarPelicula(Pelicula pelicula) {
        ResultSet result;
        int id = -1;

        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "INSERT INTO Peliculas VALUES (?,?,?,?,?,?,?)";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, pelicula.getId());
            preparedStatement.setString(2, pelicula.getNombre());
            preparedStatement.setInt(3, pelicula.getGeneroId());
            preparedStatement.setInt(4, pelicula.getAnioLanzamiento());
            preparedStatement.setString(5, pelicula.getImagenSd());
            preparedStatement.setString(6, pelicula.getImagenHd());
            preparedStatement.setString(7, pelicula.getDescripcion());

            preparedStatement.executeUpdate();

            result = preparedStatement.getGeneratedKeys();

            if (result.first()) {
                id = result.getInt(1);
            }

            preparedStatement.close();

        } //Fin de try //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return id;
    }

    public void actualizarGenero(Genero genero) {
        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "UPDATE Generos SET nombre = ? WHERE id = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setString(1, genero.getNombre());
            preparedStatement.setInt(2, genero.getId());

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } //Fin de try //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch
    }

    public void actualizarPelicula(Pelicula pelicula) {
        try {

            //Forma de insertar utilizando el Prepared Statement
            String sql = "UPDATE Peliculas SET nombre = ?, genero_id = ?, anio_lanzamiento = ?, imagen_sd = ?, imagen_hd = ?, descripcion = ? WHERE id = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setString(1, pelicula.getNombre());
            preparedStatement.setInt(2, pelicula.getGeneroId());
            preparedStatement.setInt(3, pelicula.getAnioLanzamiento());
            preparedStatement.setString(4, pelicula.getImagenSd());
            preparedStatement.setString(5, pelicula.getImagenHd());
            preparedStatement.setString(6, pelicula.getDescripcion());
            preparedStatement.setInt(7, pelicula.getId());

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } //Fin de try //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch
    }

    public void eliminarGenero(int id) {

        try {

            String sql = "DELETE FROM Generos WHERE id = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setInt(1, id);

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

    }

    public void eliminarPelicula(int id) {

        try {

            String sql = "DELETE FROM Peliculas WHERE id = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setInt(1, id);

            preparedStatement.executeUpdate();

            preparedStatement.close();

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

    }

    public List<Genero> getGeneros() {
        List generos = new ArrayList();

        try {

            String sql = "SELECT * FROM Generos";
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                Genero genero = new Genero();
                genero.setId(resultSet.getInt("id"));
                genero.setNombre(resultSet.getString("nombre"));

                generos.add(genero);

            }

        } //Fin de try //Fin de try //Fin de try //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return generos;
    }

    public List<Pelicula> getPeliculas() {
        List peliculas = new ArrayList();

        try {

            String sql = "SELECT * FROM Peliculas";
            resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {

                Pelicula pelicula = new Pelicula();
                pelicula.setId(resultSet.getInt("id"));
                pelicula.setNombre(resultSet.getString("nombre"));
                pelicula.setGeneroId(resultSet.getInt("genero_id"));
                pelicula.setAnioLanzamiento(resultSet.getInt("anio_lanzamiento"));
                pelicula.setImagenSd(resultSet.getString("imagen_sd"));
                pelicula.setImagenHd(resultSet.getString("imagen_hd"));
                pelicula.setDescripcion(resultSet.getString("descripcion"));

                peliculas.add(pelicula);

            }

        } //Fin de try //Fin de try //Fin de try //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return peliculas;
    }

    public Genero getGenero(int id) {
        Genero genero = null;
        ResultSet resultSet;

        try {
            String sql = "SELECT * FROM Generos WHERE id = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setInt(1, id);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.first()) {

                genero = new Genero();
                genero.setId(resultSet.getInt("id"));
                genero.setNombre(resultSet.getString("nombre"));
                System.out.println(genero);
            }

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return genero;
    }

    public Pelicula getPelicula(int id) {
        Pelicula pelicula = null;
        ResultSet resultSet;

        try {
            String sql = "SELECT * FROM Peliculas WHERE id = ?";

            PreparedStatement preparedStatement;

            preparedStatement = (PreparedStatement) connection.prepareStatement(sql);

            preparedStatement.setInt(1, id);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.first()) {

                pelicula = new Pelicula();
                pelicula.setId(resultSet.getInt("id"));
                pelicula.setNombre(resultSet.getString("nombre"));
                pelicula.setGeneroId(resultSet.getInt("genero_id"));
                pelicula.setAnioLanzamiento(resultSet.getInt("anio_lanzamiento"));
                pelicula.setImagenSd(resultSet.getString("imagen_sd"));
                pelicula.setImagenHd(resultSet.getString("imagen_hd"));
                pelicula.setDescripcion(resultSet.getString("descripcion"));

                System.out.println(pelicula);
            }

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al acceder a la Base de Datos");

        }//Fin de catch

        return pelicula;
    }

    /**
     * Cierra la Base de Datos.
     */
    public void cerrarBD() {

        try {

            statement.close();//Cierra el Statement.
            connection.close();//Termina la conxión con la BD.

        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al cerrar la Base de Datos");

        }//Fin de catch

    }//Fin del método cerrarBD
}//Fin de clase