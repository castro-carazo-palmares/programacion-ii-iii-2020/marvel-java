package api;

import static api.JsonUtil.json;
import entidades.Genero;
import entidades.Pelicula;
import static spark.Spark.*;

public class Main {

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public static void main(String[] args) {

        port(8080);
        Servicio servicio = new Servicio();

        // POST - Insertar un género en la BD
        post("/api/genero", (request, response) -> {
            String nombre = request.queryParams("nombre");

            if (nombre != null) {
                Genero genero = new Genero(0, nombre);
                return servicio.postGenero(genero);
            } else {
                response.status(400);
                return "Parámetro inválido";
            }
        }, json());

        // POST - Insertar una película en la BD
        post("/api/pelicula", (request, response) -> {
            String nombre = request.queryParams("nombre");
            String generoId = request.queryParams("generoId");
            String anioLanzamiento = request.queryParams("anioLanzamiento");
            String imagenSd = request.queryParams("imagenSd");
            String imagenHd = request.queryParams("imagenHd");
            String descripcion = request.queryParams("descripcion");

            try {

                if (nombre != null && generoId != null && anioLanzamiento != null && imagenSd != null && imagenHd != null && descripcion != null) {
                    Pelicula pelicula = new Pelicula(0, nombre, Integer.parseInt(generoId), Integer.parseInt(anioLanzamiento), imagenSd, imagenHd, descripcion);
                    return servicio.postPelicula(pelicula);
                } else {
                    response.status(400);
                    return "Parámetros inválidos";
                }

            } catch (NumberFormatException nfe) {
                response.status(400);
                return "Formato no válido";
            }
        }, json());

        // GET - Devuelve todos los géneros de la BD
        get("/api/genero", (request, response) -> {
            return servicio.getGeneros();
        }, json());

        // GET - Devuelve todas los películas de la BD
        get("/api/pelicula", (request, response) -> {
            return servicio.getPeliculas();
        }, json());

        // GET - Devuelve un género por el Id
        get("/api/genero/:id", (request, response) -> {
            String id = request.params(":id");

            try {
                return servicio.getGenero(Integer.parseInt(id));
            } catch (NumberFormatException nfe) {
                response.status(400);
                return "Id no válido";
            }
        }, json());

        // GET - Devuelve una película por el Id
        get("/api/pelicula/:id", (request, response) -> {
            String id = request.params(":id");

            try {
                return servicio.getPelicula(Integer.parseInt(id));
            } catch (NumberFormatException nfe) {
                response.status(400);
                return "Id no válido";
            }
        }, json());

        // PATCH - Actualiza un género por el Id
        patch("/api/genero/:id", (request, response) -> {
            String id = request.params(":id");
            String nombre = request.queryParams("nombre");

            try {
                Genero genero = new Genero(Integer.parseInt(id), nombre);
                return servicio.patchGenero(genero);
            } catch (NumberFormatException nfe) {
                response.status(400);
                return "Id no válido";
            }
        }, json());

        // PATCH - Actualiza una película por el Id
        patch("/api/pelicula/:id", (request, response) -> {
            String id = request.params(":id");
            String nombre = request.queryParams("nombre");
            String generoId = request.queryParams("generoId");
            String anioLanzamiento = request.queryParams("anioLanzamiento");
            String imagenSd = request.queryParams("imagenSd");
            String imagenHd = request.queryParams("imagenHd");
            String descripcion = request.queryParams("descripcion");

            try {

                if (nombre != null && generoId != null && anioLanzamiento != null && imagenSd != null && imagenHd != null && descripcion != null) {
                    Pelicula pelicula = new Pelicula(Integer.parseInt(id), nombre, Integer.parseInt(generoId), Integer.parseInt(anioLanzamiento), imagenSd, imagenHd, descripcion);
                    return servicio.patchPelicula(pelicula);
                } else {
                    response.status(400);
                    return "Parámetros inválidos";
                }

            } catch (NumberFormatException nfe) {
                response.status(400);
                return "Formato no válido";
            }
        }, json());

        // DELETE - Elimina un género por el Id
        delete("/api/genero/:id", (request, response) -> {
            String id = request.params(":id");

            try {
                return servicio.deleteGenero(Integer.parseInt(id));
            } catch (NumberFormatException nfe) {
                response.status(400);
                return "Formato no válido";
            }
        }, json());
        
        // DELETE - Elimina una película por el Id
        delete("/api/pelicula/:id", (request, response) -> {
            String id = request.params(":id");

            try {
                return servicio.deletePelicula(Integer.parseInt(id));
            } catch (NumberFormatException nfe) {
                response.status(400);
                return "Formato no válido";
            }
        }, json());
        
        
        //Authetication
//        before((request, response) -> {
//            String method = request.requestMethod();
//            if (method.equals("GET") || method.equals("POST") || method.equals("PUT") || method.equals("DELETE")) {
//                String authentication = request.headers("Authentication");
//                if (!"CastroCarazo".equals(authentication)) {
//                    halt(401, "Usuario no autorizado");
//                }
//            }
//        });

        //CORS
        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.type("application/json");
            response.header("Access-Control-Allow-Origin", "*");
        });
    }
}
